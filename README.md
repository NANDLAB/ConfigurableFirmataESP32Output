# Configurable Firmata ESP32 Output
To use Firmata with the ESP32, you possibly have to clone the latest version of ConfigurableFirmata from GitHub, because the library version from the Arduino IDE library manager is outdated.

Enter the `Arduino/libraries` directory and remove `ConfigurableFirmata` if present.

Then do `git clone https://github.com/firmata/ConfigurableFirmata.git`.

You also have to install the `DHT sensor library` in the library manager.
